![Logo HA](https://home-assistant.io/images/components/alexa/alexa-512x512.png)

# Home Assistant setting - YAHASG

## Why this repo ?

To keep safe and share my home assistant config.
I was glad to be able to browse all the existing one
so if this can help other while being useful for me,
let's go :)

## What can We find here ?

The current state of my Home Assistant config.
The **stable** branch is **master**, while the **develop**
branch contain functionnalities that I'm working on.

## Hardware ?

* Raspbian native with manual python virtual env install.
* Xiaomi Aquara sensors
* Zwave (soon)

## Software ?

* Around 10% Cpu Load average up to 50% when loading page.
* 105 Mo of RAM taken

I used a Let's Encrypt certificate for the HTTPS web interface, served by
a NGINX reverse proxy.

## Organisation

I directly load directory in place of putting each file into configuration.yaml by
using these two directives:

* customize: !include_dir_merge_named custom    # All the customization are in custom
* packages: !include_dir_named packages         # All the conf file are in packages

# Versions

You can find all versions in the [tags page](https://gitlab.com/linceaerian/homeassistant-config/tags)
the following details the configuration of thoses versions.

# Configuration of V2.0 

## Screenshot

### Default view

![HA Default](screenshot/hass_v2.PNG)

### Weather view

![HA Weather](screenshot/hass_v2_weather.PNG)

### People view

![HA Default](screenshot/hass_v2_people.PNG)


## Aquara Setting

`WIP`

* [Gateway settings](package/xiaomi.yaml)
* [Friendly naming](custom/xiaomi.yaml)

## Weather Setting

`WIP`

I used the beautifull settings made by Arsaboo & DetroitEE.
I use Dark Sky API to publish data on my HA.

* [Group display on main view](packages/group.yaml)
* [Weather settings](packages/weather.yaml)
* [Friendly naming](custom/weather.yaml) 
* [Update friendly naming](python_scripts/dark_sky_friendly_names.py)
* [Hide all non wanted sensors](custom/hidden.yaml)
* [Remove dark sky sensors from Logbook](logbook/logbook.yaml) 

## Tracking with Owntrack

`WIP`

I used the MQTT mosquitto approach cause I wanted to securize a bit the 
messages exchange between clients/servers and I failed to get the internal
MQTT server work with many parameters.

As TLS settings didn't worked with HA (didn't get it), I configured the MQTT
daemon on localhost for HA on 1883 and 8883 for client with Self signed certificates.

So the setting came into those files:

* [Battery JSON extract](packages/sensor.yaml)
* [Owntrack settings](packages/device_tracker.yaml)
* [MQTT config](packages/mqtt.yaml)
* [Refresh owntrack data](packages/automations.yaml)

# Configuration of V1.0 

## Screenshot

![HA](screenshot/HA1.PNG)

## Aquara Setting

* [Gateway settings](package/xiaomi.yaml)
* [Friendly naming](custom/xiaomi.yaml)

## Weather Setting

I used the beautifull settings made by Arsaboo & DetroitEE.
I use Dark Sky API to publish data on my HA.

* [Group display on main view](packages/group.yaml)
* [Weather settings](packages/weather.yaml)
* [Friendly naming](custom/weather.yaml) 
* [Update friendly naming](python_scripts/dark_sky_friendly_names.py)
* [Hide all non wanted sensors](custom/hidden.yaml)
* [Remove dark sky sensors from Logbook](logbook/logbook.yaml) 

## Tracking with Owntrack

I used the MQTT mosquitto approach cause I wanted to securize a bit the 
messages exchange between clients/servers and I failed to get the internal
MQTT server work with many parameters.

As TLS settings didn't worked with HA (didn't get it), I configured the MQTT
daemon on localhost for HA on 1883 and 8883 for client with Self signed certificates.

So the setting came into those files:

* [Battery JSON extract](packages/sensor.yaml)
* [Owntrack settings](packages/device_tracker.yaml)
* [MQTT config](packages/mqtt.yaml)
* [Refresh owntrack data](packages/automations.yaml)
